class ApplicationController < ActionController::API

  def current_user
    if decoded.present?
      #byebug
      User.find(decoded["user_id"])
      #byebug
    else
      return nil
    end
  end
  
  def decoded
    #byebug
    if auth_token.present?
      #byebug
      decoded_array = JsonWebToken.decode(auth_token)
      #byebug
      return decoded_array[0]
    else
      #byebug
      return nil
    end
  end
  
  def auth_token
    #byebug
    token = request.headers["Authorization"]
    #byebug
    return nil if token.nil?
    token.split(" ").last
    #byebug
  end
end
