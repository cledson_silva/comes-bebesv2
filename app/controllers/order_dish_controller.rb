class OrderDishController < ApplicationController
  #before_action :set_order_dish, :set_order_dishes, only: [:add_item, :remove_item]
  before_action :set_order, :set_order_dish, only: [:add_item, :remove_item]
  load_and_authorize_resource

  def add_item
    if @order.nil?
      if order_params[:id].nil?
    #byebug
      @order = Order.create(
        restaurant_id: order_params[:restaurant_id],
        client_id: current_user.id, #order_params[:client_id],
        is_done: false, 
        is_confirmed: false
      )
      else
        return render json: {Erro: "Pedido não existe"}
      end
    end
    if @orderdish.nil?
    #byebug
      if Dish.find(order_params[:order_dish][:dish_id]).restaurant_id != @order.restaurant_id
        return render json: {Erro: "O prato não faz parte do restaurante"}
      else
        @orderdish = OrderDish.create(
          order_id: @order.id,
          dish_id: order_params[:order_dish][:dish_id]
        )
      end
    end
    byebug
    @orderdish.quantity += 1
    @orderdish.partial_value = (@orderdish.quantity * Dish.find(order_params[:order_dish][:dish_id]).value)
    if @orderdish.save
      
      @order.update(total_value: OrderDish.where(order_id: @order.id).sum(:partial_value))
      render json: @order
    else
      render json: @order.errors, status: 422
    end
  end


  def remove_item
    if @order.nil?
      return render json: {errors: "Pedido não existe"}
    else  
      if @orderdish.nil?
        
        return render json: {Erro:"Item não está no pedido"}
      end
    end
    byebug
    @orderdish.quantity -= 1
    if @orderdish.quantity > 0
      @orderdish.partial_value = (@orderdish.quantity * Dish.find(order_params[:order_dish][:dish_id]).value)
      if @orderdish.save
        @order.update(total_value: OrderDish.where(order_id: @order.id).sum(:partial_value))
        render json: @order
      else
        render json: @order
      end
    else
      @orderdish.destroy
      @order.update(total_value: OrderDish.where(order_id: @order.id).sum(:partial_value))
      if OrderDish.where(order_id: @order.id)==[]
        @order.destroy
      else
        render json: @order
      end
    end
  end

  def set_order
    @order = Order.find(order_params[:id]) rescue nil
    #byebug
  end

  def set_order_dish
    @orderdish = OrderDish.find_by(
      order_id: @order.id, 
      dish_id: order_params[:order_dish][:dish_id]) rescue nil
  end

=begin   def set_order_dish
    @orderdish = OrderDish.find_by(
      order_id: cart_params[:order_id], 
      dish_id: cart_params[:dish_id])
  end

  def set_order_dishes
    @orderdishes = OrderDish.where(
      order_id: cart_params[:order_id])
  end

=end
=begin   
  def order_dish_params
    params.require(:order_dish).permit(
        :order_id, 
        :dish_id, 
        :quantity,
        :partial_value 
        )
    end
    
  def set_order
    
  end 
=end 
  def order_params
    #params.require(:order).permit!
    params.require(:order).permit(
          :id,
          :restaurant_id,
          order_dish: [:dish_id]
        )  
  end
end
