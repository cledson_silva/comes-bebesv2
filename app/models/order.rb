class Order < ApplicationRecord
  #has_many :dishes, through: :order_dishes
  has_many :order_dishes, dependent: :destroy
  belongs_to :client, class_name:"User"
  belongs_to :deliveryman, class_name:"User", optional: true
  belongs_to :restaurant

  #belongs_to :restaurant

  # orders validations
  validates :client_id, :restaurant_id, presence:true 

  
end
