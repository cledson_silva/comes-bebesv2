class Restaurant < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :owner, class_name: "User"
  has_many :dishes, dependent: :destroy
  has_many :orders, dependent: :destroy
  #has_many :orders, dependent: :destroy

  # restaurants validations
  validates :name, :foodType, :openingHour, :address, :cnpj, :owner_id, presence:true
  validates :owner_id, :cnpj, uniqueness:true

end

  